package com;


import com.task5.DayOfWeek;
import com.task7.TriangleChecker;

public class Main {

    public static void main(String[] args) {

        System.out.println(TriangleChecker.triangle(1.35, 2.54, 4.365));
        System.out.println(TriangleChecker.triangle(5.44, 4.13, 6.111));

        DayOfWeek[] dayOfWeeks = new DayOfWeek[7];
        dayOfWeeks[0] = new DayOfWeek((byte) 1, "Monday");
        dayOfWeeks[1] = new DayOfWeek((byte) 2, "Tuesday");
        dayOfWeeks[2] = new DayOfWeek((byte) 3, "Wednesday");
        dayOfWeeks[3] = new DayOfWeek((byte) 4, "Thursday");
        dayOfWeeks[4] = new DayOfWeek((byte) 5, "Friday");
        dayOfWeeks[5] = new DayOfWeek((byte) 6, "Saturday");
        dayOfWeeks[6] = new DayOfWeek((byte) 7, "Sunday");
        for (DayOfWeek dw : dayOfWeeks) {
            System.out.println(dw.getDay() + " " + dw.getDayOfWeek());
        }

    }
}
