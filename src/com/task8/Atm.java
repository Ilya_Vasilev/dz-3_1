package com.task8;

public class Atm {
    private double currency;
    private static int count;


    public Atm(double currency) {
        this.currency = currency;
        count++;
    }

    public double dollars(double dollars) {
        return dollars * currency;
    }

    public double rubles(double rubles) {
        return rubles * currency;
    }

    public static int counter() {
        return count;
    }
}