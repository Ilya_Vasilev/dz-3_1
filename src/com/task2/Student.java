package com.task2;


import java.util.Arrays;

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    //При создании студента, задаем массиву оценок длину 10 (из условия задачи)
    public Student() {

    }

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    /**
     * Метод считает оценки в массиве оценок, которые не равны нулю, создает новый массив с ненулевыми
     * оценками и возвращает его.
     */
    public int[] getGrades() {
        int count = 0;
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] == 0) {
                count++;
            }
        }

        int[] temp = new int[grades.length - count];
        System.arraycopy(grades, 0, temp, 0, grades.length - count);
        return temp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Метод принимает массив оценок, копирует его в поле оценок grades длиной 10.
     */
    public void setGrades(int[] grades) {
        this.grades = Arrays.copyOf(grades, 10);
    }

    /**
     * Метод добавляет оценку в существующий массив оценок.
     * Если последний элемент массива не ноль, сдвигает массив влево на 1 и добавляет новую оценку в конец массива.
     * Если последний элемент массива ноль, то ищет ноль с начала массива и присвает значение новой оценки ему.
     */
    public void addGrades(int newGrade) {
        if (grades[grades.length - 1] == 0) {
            for (int i = 0; i < grades.length; i++) {
                if (grades[i] == 0) {
                    grades[i] = newGrade;
                    return;
                }
            }
        } else {
            for (int i = 0; i < grades.length - 1; i++) {
                grades[i] = grades[i + 1];
            }
            grades[grades.length - 1] = newGrade;
        }
    }

    /**
     * Метод считает ненулевые оценки в массиве оценок, суммирует их и возвращает среднюю оценку с матем. округлением.
     */
    public double averageGrade() {
        double sum = 0;
        int count = 0;

        for (int grade : grades) {
            if (grade != 0) {
                sum += grade;
                count++;
            }
        }
        return (Math.round(sum / count * 10)) / 10.0;
    }
}
