package com.task1;

import java.util.Random;

public class Cat {
    Random rand = new Random();

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        switch (rand.nextInt(3)) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }
    }
}
